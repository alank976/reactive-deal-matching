package alan.projects.reactivedealmatching;

import alan.projects.reactivedealmatching.csv.CsvService;
import alan.projects.reactivedealmatching.domain.deal.DealService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.stream.Collectors;

@RestController
@RequestMapping("/match")
@AllArgsConstructor
public class MatchingController {
    private final DealService dealService;
    private final CsvService csvService;

    @GetMapping
    public String match() {
        dealService.retrieveDealsFromSomewhere();
        return csvService.getCsvLines().stream()
                .collect(Collectors.joining("\n"));
    }
}
