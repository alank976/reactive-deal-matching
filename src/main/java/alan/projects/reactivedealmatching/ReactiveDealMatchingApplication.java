package alan.projects.reactivedealmatching;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReactiveDealMatchingApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReactiveDealMatchingApplication.class, args);
	}
}
