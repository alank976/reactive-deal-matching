package alan.projects.reactivedealmatching.csv;

import lombok.Getter;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Getter
public class CsvService {
    private final List<String> csvLines = new ArrayList<>();

    public void write(String content) {
        csvLines.add(content);
    }
}
