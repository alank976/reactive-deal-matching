package alan.projects.reactivedealmatching.domain.deal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;

@Getter
@AllArgsConstructor
@Data
@Builder
public class Deal {
    private String dealId;
    private String center;
    private String way;
    private String instrumentId;
    private double price;
    private double quantity;

}
