package alan.projects.reactivedealmatching.domain.deal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public class DealEvent {
    private String eventId;
    private Deal deal;
}
