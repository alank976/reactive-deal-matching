package alan.projects.reactivedealmatching.domain.deal;

import alan.projects.reactivedealmatching.csv.CsvService;
import alan.projects.reactivedealmatching.domain.deal.matching.DealEventSubscriber;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;
import java.util.concurrent.SubmissionPublisher;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
@Slf4j
public class DealService {
    private final SubmissionPublisher<DealEvent> dealEventSubmissionPublisher;
    private final CsvService csvService;
    private static final List<String> mockScopeCenters = List.of("C1", "C2");

    public DealService(CsvService csvService) {
        this.dealEventSubmissionPublisher = new SubmissionPublisher<>();
        this.csvService = csvService;
        mockScopeCenters.stream()
                .map(List::of)
                .map(centers -> new DealEventSubscriber(csvService, centers))
                .forEach(this.dealEventSubmissionPublisher::subscribe);
    }

    public void retrieveDealsFromSomewhere() {
        Random rand = new Random();
        // Mock deal source like this
        List<Deal> deals = rand.ints(10, 1, 100000)
                .mapToObj(d -> Deal.builder()
                        .dealId(Double.toString(d))
                        .instrumentId(Double.toString(d % 5))
                        .center(d % 2 == 0 ? "C1" : "C2")
                        .way(d % 2 == 0 ? "B" : "S")
                        .price(1d)
                        .quantity(Math.abs(d % 10))
                        .build()

                )
                .collect(Collectors.toList());

        IntStream.range(0, deals.size())
                .parallel()
                .peek(i -> {
                    try {
                        Thread.sleep(rand.nextInt(1000));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                })
                .mapToObj(i -> new DealEvent(Integer.toString(i), deals.get(i)))
                .forEach(dealEventSubmissionPublisher::submit);
        dealEventSubmissionPublisher.close();
    }

}
