package alan.projects.reactivedealmatching.domain.deal.matching;

import alan.projects.reactivedealmatching.csv.CsvService;
import alan.projects.reactivedealmatching.domain.deal.Deal;
import alan.projects.reactivedealmatching.domain.deal.DealEvent;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Flow;

@Slf4j
public class DealEventSubscriber implements Flow.Subscriber<DealEvent> {
    private final CsvService csvService;
    private final List<String> centers;
    private Flow.Subscription subscription;
    private Map<String, MatchingResidual> workingUnitByInstrumentId;

    public DealEventSubscriber(CsvService csvService, List<String> centers) {
        this.csvService = csvService;
        this.centers = centers;
    }

    @Override
    public void onSubscribe(Flow.Subscription subscription) {
        this.subscription = subscription;
        subscription.request(1);
    }

    @Override
    public void onNext(DealEvent dealEvent) {
        if (centers.contains(dealEvent.getDeal().getCenter())) {
            csvService.write(dealEvent.getDeal().toString());
        }
        subscription.request(1);
    }

    @Override
    public void onError(Throwable throwable) {
        log.error("error in subscriber", throwable);
    }

    @Override
    public void onComplete() {
        csvService.write("Unmatched deals");
    }

    private static class MatchingResidual {
        private String way;
        private Map<Deal, Double> quantityByDeal;
    }
}
